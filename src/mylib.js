/** Basic arithmetic operations. */
const mylib = {
  /** Multiline arrow function for addition. */
  add: (a, b) => {
    const sum = a + b;
    return sum;
  },
  /** Multiline arrow function without intermediary variables for subtraction. */
  subtract: (a, b) => {
    return a - b;
  },
  /** Arrow function for division with divisor check. */
  divide: (dividend, divisor) => {
    if (divisor == 0) {
      throw new Error("Can't divide by zero.");
    }
    return dividend / divisor;
  },
  /** Regular function for multiplication. */
  multiply: function (a, b) {
    return a * b;
  },
  /** Return a random number between 1-100. */
  random: () => {
    return Math.floor(Math.random() * 100) + 1;
  }
};

module.exports = mylib;