const mylib = require("./mylib");
const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

/** Adds two numbers together. */
app.get("/add", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(mylib.add(a, b).toString());
});

/** Subtracts the second number from the first number. */
app.get("/subtract", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(mylib.subtract(a, b).toString());
});

/** Divides the first number by the second number. */
app.get("/divide", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(mylib.divide(a, b).toString());
});

/** Multiplies the two numbers together. */
app.get("/multiply", (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(mylib.multiply(a, b).toString());
});

/** Returns a random number between 1-100. */
app.get("/random", (req, res) => {
  res.send(mylib.random().toString());
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
