const expect = require("chai").expect;
const mylib = require("../src/mylib");

describe("Some tests in spec.js file 2", () => {
  let randomNumber = undefined;

  before(() => {
    // Initialisation
    console.log("Initialising tests.");
    randomNumber = mylib.random();
  });
  // Division tests
  it("Can divide 14 by 7", () => {
    expect(mylib.divide(14, 7)).to.equal(2, "14 / 7 not equalling 2!");
  });
  it("Can't divide a random number by 0", () => {
    expect(function () {
      mylib.divide(randomNumber, 0);
    }).to.throw(Error);
  });
  // Multiplication tests
  it("Can multiply 16 by 43", () => {
    expect(mylib.multiply(16, 43)).to.equal(688, "16 * 43 not equalling 688!");
  });
  after(() => {
    // Cleanup
    // Reset random number to 0 after testing has finished
    randomNumber = 0;
    console.log("Testing completed.");
  });
});
