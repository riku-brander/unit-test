const expect = require("chai").expect;
const mylib = require("../src/mylib");

describe("Some tests in spec.js file 1", () => {
  let randomNumber = undefined;

  before(() => {
    // Initialisation
    console.log("Initialising tests.");
    randomNumber = mylib.random();
  });
  // Random number tests
  it("Creates a random number between 1-100", () => {
    expect(randomNumber).to.be.above(0, "Random number is smaller than 1!");
    expect(randomNumber).to.be.below(101, "Random number is bigger than 100!");
  });
  // Addition tests
  it("Can add 8 and 12 together", () => {
    expect(mylib.add(8, 12)).to.equal(20, "8 + 12 not equalling 20!");
  });
  // Subtraction tests
  it("Can subtract 7 from 11", () => {
    expect(mylib.subtract(11, 7)).to.equal(4, "11 - 7 not equalling 4!");
  });
  it("Can subtract 5 from a random number", () => {
    expect(mylib.subtract(randomNumber, 5)).to.equal(randomNumber - 5, "Subtraction fails with a random number!");
  });
  afterEach(() => {
    // Test that the random number has not been modified to go below 0
    expect(randomNumber).to.be.above(0, "Random number changed to below zero!");
  });
  after(() => {
    // Cleanup
    // Reset random number to 0 after testing has finished
    randomNumber = 0;
    console.log("Testing completed.");
  });
});
