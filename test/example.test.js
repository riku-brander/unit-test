const expect = require("chai").expect;
const mylib = require("../src/mylib");

describe("Our first unit tests", () => {
  let randomNumber = undefined;

  before(() => {
    // Initialisation
    console.log("Initialising tests.");
    randomNumber = mylib.random();
  });
  // Random number tests
  it("Creates a random number between 1-100", () => {
    expect(randomNumber).to.be.above(0, "Random number is smaller than 1!");
    expect(randomNumber).to.be.below(101, "Random number is bigger than 100!");
  });
  // Addition tests
  it("Can add 1 and 2 together", () => {
    expect(mylib.add(1, 2)).to.equal(3, "1 + 2 not equalling 3!");
  });
  // Subtraction tests
  it("Can subtract 3 from 5", () => {
    expect(mylib.subtract(5, 3)).to.equal(2, "5 - 3 not equalling 2!");
  });
  it("Can subtract 1 from a random number", () => {
    expect(mylib.subtract(randomNumber, 1)).to.equal(randomNumber - 1, "Subtraction fails with a random number!");
  });
  // Division tests
  it("Can divide 8 by 2", () => {
    expect(mylib.divide(8, 2)).to.equal(4, "8 / 2 not equalling 4!");
  });
  it("Can't divide 5 by 0", () => {
    expect(function () {
      mylib.divide(5, 0);
    }).to.throw(Error);
  });
  // Multiplication tests
  it("Can multiply 5 by 6", () => {
    expect(mylib.multiply(5, 6)).to.equal(30, "5 * 6 not equalling 30!");
  });
  afterEach(() => {
    // Test that the random number has not been modified to go below 0
    expect(randomNumber).to.be.above(0, "Random number changed to below zero!");
  });
  after(() => {
    // Cleanup
    // Reset random number to 0 after testing has finished
    randomNumber = 0;
    console.log("Testing completed.");
  });
});
